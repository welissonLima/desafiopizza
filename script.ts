var arrayPesquisa = [];

var btnSalvar = document.getElementById("btnSalvar");
btnSalvar.addEventListener("click", function(){
    let formulario = document.getElementById("formPizza") as HTMLFormElement;

    let pizza: object = pegarDados(formulario);

    arrayPesquisa.push(pizza);

    formulario.reset();
});

var btnRelatorio = document.getElementById("btnRelatorio");
btnRelatorio.addEventListener("click", function(){
    arrayPesquisa = ordenarArray();

    let tHead: HTMLElement = document.getElementById("idThead");
    tHead.innerHTML = "";
    tHead.appendChild(montarThead());

    let tBody: HTMLElement = document.getElementById("idTbody");
    tBody.innerHTML = "";

    montarTbody(tBody);
});

function ordenarArray(): any[]{
    for(var i=0; i < arrayPesquisa.length-1; i++){
        for(var j=i+1; j < arrayPesquisa.length; j++){
            if(parseFloat(arrayPesquisa[i].precoPorCm) > parseFloat(arrayPesquisa[j].precoPorCm)){
                trocarPosicao(arrayPesquisa, j, i);
            }
        }    
    }

    return arrayPesquisa;
}

function trocarPosicao(array: any[], from: number, para: number){
    array.splice(para, 0, array.splice(from, 1)[0]);
    return array;
}

function montarThead(): HTMLTableRowElement{
    let titulos: HTMLTableRowElement = document.createElement("tr");
    
    titulos.appendChild(montarTh("Nome"));
    titulos.appendChild(montarTh("Tamanho"));
    titulos.appendChild(montarTh("Preço"));
    titulos.appendChild(montarTh("R$ p/cm2"));
    titulos.appendChild(montarTh("Diferença %"))

    return titulos;
}

function montarTbody(tBody: HTMLElement){
    for(var i = 0; i < arrayPesquisa.length; i++){
        var linhaTd: HTMLTableRowElement = document.createElement("tr");

        linhaTd.appendChild(montarTd(arrayPesquisa[i].nome));
        linhaTd.appendChild(montarTd(arrayPesquisa[i].tamanho));
        linhaTd.appendChild(montarTd(formatarValor(arrayPesquisa[i].preco)));
        linhaTd.appendChild(montarTd(formatarValor(arrayPesquisa[i].precoPorCm)));
        linhaTd.appendChild(calcularDiferenca(arrayPesquisa[i].precoPorCm));
        
        tBody.appendChild(linhaTd);
    }
}

function formatarValor(valor: number): string{
    let valorFormatado: string = valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    return valorFormatado;
}

function calcularDiferenca(preco: number): HTMLTableCellElement{
    let menorPreco: number = arrayPesquisa[0].precoPorCm;
    
    for(var i = 0; i < arrayPesquisa.length; i++){
        if(arrayPesquisa[i].precoPorCm < menorPreco )
            menorPreco = arrayPesquisa[i].precoPorCm;
    }

    if(menorPreco == preco){
        return montarTd("Melhor");
    }

    return montarTd(`+${(((preco * 100)/menorPreco)-100).toFixed(2)}%`);
}

function montarTd(dado: string): HTMLTableCellElement{
    let coluna : HTMLTableCellElement = document.createElement("td");
    coluna.textContent = dado;

    return coluna;
}

function montarTh(dado: string): HTMLTableCellElement{
    let titulo: HTMLTableCellElement = document.createElement("th");
    titulo.textContent = dado;

    return titulo;
}

function pegarDados(formulario: HTMLFormElement): object {
    let pizza = {
        nome : ((formulario.idNome) as HTMLInputElement).value,
        tamanho : Number(((formulario.idTamanho) as HTMLInputElement).value),
        preco : Number(((formulario.idPreco) as HTMLInputElement).value),
        area : 0,
        precoPorCm : 0,
        diferenca : 0
    }

    pizza.area = Number(3.14 * ((pizza.tamanho / 2)**2));
    pizza.precoPorCm = Number(pizza.preco / pizza.area);

    return pizza;
}